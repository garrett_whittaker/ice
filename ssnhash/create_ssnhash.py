#!/usr/bin/python
import sys, string, re

BEGINNING_KEY = {
    '0':'Z',
    '1':'A',
    '2':'B',
    '3':'C',
    '4':'D',
    '5':'E',
    '6':'F',
    '7':'G',
    '8':'H',
    '9':'I'
}

def create_hash(ssn):
    ssnhash = ''

    beginning = ssn[:3] # Get first 3 numbers of ssn
    for char in beginning: # Convert first 3 numbers based on BEGINNING_KEY
        ssnhash += BEGINNING_KEY.get(char)

    end = int(ssn[3:]) # Get last 6 numbers of ssn
    hashed_end = '' # Store our 'hash' of the last 6 numbers
    while end > 0: # Manually base35 encode last 6 numbers
        hashed_end = string.printable[end % 35] + hashed_end
        end //= 35
    formatted = hashed_end.zfill(4) # Add leading 0's if shorter than 4 chars
    ssnhash += str(formatted).upper() # Combine all chars and make them uppercase

    return ssnhash

if __name__ == "__main__":
    try:
        CLEAN = str(re.sub(r"\D", "", sys.argv[1]))
        if len(CLEAN) == 9:
            print(create_hash(CLEAN))
        else:
            print("Invalid input")
    except:
        print("Proper usage:\n$ python " + sys.argv[0] + " <ssn>\n")
