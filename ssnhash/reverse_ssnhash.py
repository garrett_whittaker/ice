#!/usr/bin/python
import sys

beginning_key = {
    'Z':'0',
    'A':'1',
    'B':'2',
    'C':'3',
    'D':'4',
    'E':'5',
    'F':'6',
    'G':'7',
    'H':'8',
    'I':'9'
}

def crack_hash(input):
    cracked = ''

    beginning = input[:3] # Get first 3 chars of 'hash'
    for char in beginning: # Convert back first 3 numbers based on beginning_key
        cracked += beginning_key.get(char)

    end = input[3:] # Get last 4 chars of 'hash'
    base35_decode = int(end, 35) # Base35 decode the chars
    formatted = format(base35_decode, '06') # Keep leading 0's
    cracked += str(formatted)

    print cracked

if __name__ == "__main__":
    try:
        if len(sys.argv[1]) == 7:
            crack_hash(sys.argv[1].upper())
        else:
            print "Invalid input"
    except:
        print "Proper usage:\n$ python reverse_ssnhash.py <hash>\n"
